import { schema } from 'nexus'

schema.objectType({
  name: 'User',
  definition(t) {
    t.string('name')
  }
})

schema.extendType({
  type: 'Query',
  definition(t) {
    t.field('viewer', {
      nullable: true,
      type: 'User',
      resolve(_root, _args, ctx) {
        return ctx.token.userName
      }
    })
  }
})
