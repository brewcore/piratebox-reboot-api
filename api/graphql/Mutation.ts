import { sign } from 'jsonwebtoken'
import { schema } from 'nexus'

export const Mutation = schema.mutationType({
  definition(t) {
    t.field('login', {
      type: 'AuthPayload',
      args: {
        name: schema.stringArg({ nullable: false }),
      },
      resolve: async (_parent, { name }, _context) => {
        return {
          token: sign({ userName: name }, process.env.APPSECRET || '')
        }
      },
    })
  },
})
