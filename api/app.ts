import { use } from 'nexus'
import { prisma } from 'nexus-plugin-prisma'
import { auth } from 'nexus-plugin-jwt-auth'

use(prisma())
 
// Define the paths you'd like to protect
const protectedPaths = [
  'Query.viewer'
]

if (!process.env.APPSECRET) {
  console.error('Pease define an environment variable called APPSECRET with a random key for authentication use.')
}

// Enables the JWT Auth plugin with permissions
use(auth({
  appSecret: process.env.APPSECRET, // optional if using custom verify function
  protectedPaths
}))
