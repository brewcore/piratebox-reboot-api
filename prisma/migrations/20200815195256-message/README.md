# Migration `20200815195256-message`

This migration has been generated by Tyler Golden at 8/15/2020, 3:52:56 PM.
You can check out the [state of the schema](./schema.prisma) after the migration.

## Database Steps

```sql
PRAGMA foreign_keys=OFF;

CREATE TABLE "new_Message" (
"id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
"author" TEXT NOT NULL,
"createdAt" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
"message" TEXT NOT NULL)

INSERT INTO "new_Message" ("id", "author") SELECT "id", "author" FROM "Message"

PRAGMA foreign_keys=off;
DROP TABLE "Message";;
PRAGMA foreign_keys=on

ALTER TABLE "new_Message" RENAME TO "Message";

PRAGMA foreign_key_check;

PRAGMA foreign_keys=ON;
```

## Changes

```diff
diff --git schema.prisma schema.prisma
migration 20200815194335-create..20200815195256-message
--- datamodel.dml
+++ datamodel.dml
@@ -5,14 +5,16 @@
 //   - Prisma Schema docs https://pris.ly/d/prisma-schema
 datasource sqlite {
   provider = "sqlite"
-  url = "***"
+  url = "***"
 }
 model Message {
   id Int @id @default(autoincrement())
   author String
+  createdAt DateTime @default(now())
+  message String
 }
 generator prisma_client {
   provider = "prisma-client-js"
```


