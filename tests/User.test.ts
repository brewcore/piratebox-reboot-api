import { createTestContext } from './__helpers'

const ctx = createTestContext()

// TODO write real tests

it('ensures that a draft can be created and published', async () => {
  // Publish the previously created draft
  const publishResult = await ctx.client.send(`
    mutation publishDraft($draftId: Int!) {
      publish(draftId: $draftId) {
        id
        title
        body
        published
      }
    }
  `,
  { draftId: 1 }
  )

  // Snapshot the published draft and expect `published` to be true
  expect(publishResult).toMatchInlineSnapshot(`
    Object {
      "publish": Object {
        "body": "...",
        "id": 1,
        "published": true,
        "title": "Nexus",
      },
    }
  `)
})
